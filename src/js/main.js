
class Test {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }


    toString() {
        return `(${this.x},  ${this.y})`;
    }

    static test1() {
        return "hello world";
    }
}

let test = new Test(0,1);
console.log(Test.test1());
console.log(test.toString());

setTimeout(() => {
    console.log("ES6 FTW");
}, 1000);
