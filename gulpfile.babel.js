'use strict';
import gulp from 'gulp';
import del from 'del';
import browserSync from 'browser-sync';
import gulpLoadPlugins from 'gulp-load-plugins';


require("babel-register");

const fastMode = false;

const plugin = gulpLoadPlugins();

const dirs = {
    src: 'src',
    dist: 'dist'
};

const sassPaths = {
    src: `${dirs.src}/scss/*.scss`,
    fullSrc: `${dirs.src}/scss/**/*.scss`,
    dist: `${dirs.dist}/css/`
};

const jsPaths = {
    src: `${dirs.src}/js/**/*.js`,
    dist: `${dirs.dist}/js/`
};

const fontPaths = {
    src: `${dirs.src}/fonts/**/*`,
    dist: `${dirs.dist}/fonts/`
};

const assetsPaths = {
    src: `${dirs.src}/assets/**/*`,
    dist: `${dirs.dist}/`
};

const htmlPaths = {
    src: `${dirs.src}/templates/**/*.html`,
    dist: `${dirs.dist}/`
};

// css and sass files
gulp.task('styles', () => {
    return gulp.src
    ([
        sassPaths.src,
    ])
        .pipe(plugin.plumber())
        .pipe(plugin.if(!fastMode, plugin.sourcemaps.init()))
        .pipe(plugin.sass())
        .pipe(plugin.concat(`style.css`))
        .pipe(plugin.if(!fastMode, plugin.replace('/*!', '/*')))
        .pipe(plugin.if(!fastMode, plugin.cleanCss()))
        .pipe(plugin.if(!fastMode, plugin.autoprefixer('last 4 version')))
        .pipe(plugin.rename({suffix: '.min'}))
        .pipe(plugin.if(!fastMode, plugin.sourcemaps.write(`.`)))
        .pipe(gulp.dest(sassPaths.dist))
});

// js files
gulp.task('scripts', () => {
    let condition = file => file.path.replace(__dirname, '').substr(1).startsWith(dirs.src);
    gulp.src
    ([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/popper.js/dist/umd/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        jsPaths.src
    ])
        .pipe(plugin.plumber())
        .pipe(plugin.if(!fastMode, plugin.sourcemaps.init()))
        .pipe(plugin.if(condition, plugin.babel({presets: ['env']})))
        .pipe(plugin.concat(`script.js`))
        .pipe(plugin.if(!fastMode, plugin.uglify()))
        .pipe(plugin.rename({suffix: '.min'}))
        .pipe(plugin.if(!fastMode, plugin.sourcemaps.write(`.`)))
        .pipe(gulp.dest(jsPaths.dist))
});


// html files
gulp.task('template', () => {
    gulp.src(htmlPaths.src)
        .pipe(gulp.dest(htmlPaths.dist))
        .pipe(plugin.htmlmin({collapseWhitespace: true}))
});


// indent fonts
gulp.task('font', () => {
    return gulp.src
    ([
        './node_modules/font-awesome/fonts/*.woff2',
        './node_modules/samim-font/dist/Samim.woff',
        fontPaths.src
    ])
        .pipe(gulp.dest(fontPaths.dist))
});


// indent assets
gulp.task('assets', () => {
    return gulp.src([assetsPaths.src])
        .pipe(gulp.dest(assetsPaths.dist))
});


// clean project
gulp.task('clean', () => {
    del([dirs.src]);
});


// browser-sync configs
gulp.task('browser-sync', () => {
    browserSync.init(null, {
        open: true,
        server: {
            baseDir: dirs.dist
        }
    });
});


// build task
gulp.task('build', () => {
    gulp.start(['font', 'assets', 'scripts', 'styles', 'template']);
});


// task for start build and watch project
gulp.task('start', () => {
    gulp.start(['build', 'browser-sync']);
    gulp.watch([assetsPaths.src], ['assets']).on('change', browserSync.reload);
    gulp.watch([fontPaths.src], ['font']).on('change', browserSync.reload);
    gulp.watch([sassPaths.fullSrc], ['styles']).on('change', browserSync.reload);
    gulp.watch([jsPaths.src], ['scripts']).on('change', browserSync.reload);
    gulp.watch([htmlPaths.src], ['template']).on('change', browserSync.reload);
});


// default task
gulp.task('default', ['start']);
